package com.example.javaspringapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaspringappApplication {

    public static void main(String[] args) {
        SpringApplication.run(JavaspringappApplication.class, args);
    }

}
